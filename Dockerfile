FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
COPY ./target/movie-search-api-0.0.1-SNAPSHOT.jar movie-search-api.jar
CMD ["java", "-jar", "movie-search-api.jar"]
