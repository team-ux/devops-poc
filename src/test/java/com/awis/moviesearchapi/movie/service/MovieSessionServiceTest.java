package com.awis.moviesearchapi.movie.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import org.joda.time.DateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MovieSessionServiceTest {

    private static final List<String> sessionsHours = Arrays.asList(
        "10:00:00",
        "11:00:00",
        "15:00:00",
        "17:00:00",
        "19:00:00",
        "20:00:00",
        "21:00:00",
        "22:00:00"
    );
    Date startDate = new SimpleDateFormat("yyyy-mm-dd").parse("2020-12-05");
    Date endDate = new SimpleDateFormat("yyyy-mm-dd").parse("2020-12-12");

    @Autowired
    private MovieSessionService movieSessionService;

    MovieSessionServiceTest() throws ParseException {}

    @BeforeEach
    void setUp() {}

    @AfterEach
    void tearDown() {}

    @Test
    void pickARandomSessionTime() {
        Random rng = new Random();
        List<Integer> list = Arrays
            .stream(sessionsHours.get(rng.nextInt(sessionsHours.size())).split(":"))
            .map(Integer::valueOf)
            .collect(Collectors.toList());
        assertEquals(list.size(), 3);
    }

    @Test
    void generateRandomSessionsPerWeek() {
        int weeks = movieSessionService.countNumberOfWeek(startDate, endDate);
        Set<Integer> days = movieSessionService.generateRandomDay();
        DateTime start = new DateTime(startDate);
        days.forEach(
            day -> {
                System.out.println("before" + start.plusDays(day));
                List<Integer> times = movieSessionService.pickARandomSessionTime();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startDate);
                calendar.add(Calendar.DATE, day);
                calendar.set(Calendar.HOUR_OF_DAY, times.get(0));
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                System.out.println(new Timestamp(calendar.getTimeInMillis()));
            }
        );
    }
}
