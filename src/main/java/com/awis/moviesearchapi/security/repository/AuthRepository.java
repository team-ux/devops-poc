package com.awis.moviesearchapi.security.repository;

import com.awis.moviesearchapi.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthRepository extends JpaRepository<User, Integer> {}
