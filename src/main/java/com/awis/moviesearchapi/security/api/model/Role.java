package com.awis.moviesearchapi.security.api.model;

public enum Role {
    MOVIE_OWNER,
    ADMIN
}
