package com.awis.moviesearchapi.security.api.resource;

import com.awis.moviesearchapi.security.api.model.Credential;
import com.awis.moviesearchapi.security.service.AuthService;
import com.awis.moviesearchapi.security.service.JwtService;
import com.awis.moviesearchapi.user.entity.User;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/auth")
public class AuthenticationResource {

    private final AuthService authService;

    private final JwtService jwtService;

    @Autowired
    public AuthenticationResource(AuthService authService, JwtService jwtService) {
        this.authService = authService;
        this.jwtService = jwtService;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response authenticateUser(Credential credential) {
        try {
            // Authenticate the user using the credentials provided
            User user = authenticate(credential);
            // Issue a token for the user
            String token = jwtService.issueToken(user);

            // Return the token on the response
            return Response.ok(token).build();
        } catch (Exception e) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    private User authenticate(Credential credential) throws Exception {
        // Authenticate against a database, LDAP, file or whatever
        // Throw an Exception if the credentials are invalid
        return authService.authWithEmailAndPassword(credential);
    }
}
