package com.awis.moviesearchapi.security.service;

import com.awis.moviesearchapi.user.entity.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class JwtService {

    @Value("${movie.token.secret}")
    private String secret;

    @Value("${movie.token.expiration}")
    private long expiration;

    public String issueToken(User user) {
        // Issue a token (can be a random String persisted to a database or a JWT token)
        // The issued token must be associated to a user
        // Return the issued token
        System.out.println(secret);
        if (user.getMovieTheatre() == null) {
            return Jwts
                .builder()
                .setSubject(user.getEmail())
                .setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
        }
        return Jwts
            .builder()
            .setSubject(user.getEmail())
            .claim("movie-theatre", user.getMovieTheatre().getIdMovieTheatre())
            .setExpiration(new Date(System.currentTimeMillis() + expiration * 1000))
            .signWith(SignatureAlgorithm.HS512, secret)
            .compact();
    }

    public void validateToken(String token) throws Exception {
        // Check if the token was issued by the server and if it's not expired
        // Throw an Exception if the token is invalid
        final String username = getUsernameFromToken(token);
        final Date expiration = getExpirationDate(token);
        final Integer movieTheatre = getMovieTheatreId(token);
        if (!expiration.after(new Date(System.currentTimeMillis()))) {
            throw new Exception("expi");
        }
    }

    public String getUsernameFromToken(String token) {
        String username;
        try {
            username = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
        } catch (Exception e) {
            username = null;
        }
        return username;
    }

    public Integer getMovieTheatreId(String token) {
        Integer movieTheatreId;
        try {
            movieTheatreId = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().get("movie-theatre", Integer.class);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            movieTheatreId = null;
        }
        return movieTheatreId;
    }

    public Date getExpirationDate(String token) {
        Date expiration;
        try {
            expiration = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }
}
