package com.awis.moviesearchapi.security.service;

import com.awis.moviesearchapi.security.api.model.Credential;
import com.awis.moviesearchapi.user.entity.User;
import com.awis.moviesearchapi.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private final UserRepository userRepository;

    @Autowired
    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User authWithEmailAndPassword(Credential credential) throws Exception {
        User user = this.userRepository.getUserByEmailAndPassword(credential.getUsername(), credential.getPassword());
        if (user != null) return user; else throw new Exception("invalid credentials");
    }
}
