package com.awis.moviesearchapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieSearchApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MovieSearchApiApplication.class, args);
    }
}
