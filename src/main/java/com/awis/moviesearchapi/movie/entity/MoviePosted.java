package com.awis.moviesearchapi.movie.entity;

import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "movie_posted", schema = "movie-search")
@IdClass(MoviePostedPK.class)
public class MoviePosted {

    private Integer idMovie;
    private Integer idMovieTheatre;

    private Movie movie;
    private MovieTheatre movieTheatre;

    private Date startDate;
    private Date endDate;

    public MoviePosted(Integer idMovie, Integer idMovieTheatre) {
        this.idMovie = idMovie;
        this.idMovieTheatre = idMovieTheatre;
    }

    public MoviePosted() {}

    @ManyToOne
    @JoinColumn(name = "id_movie", referencedColumnName = "id_movie", nullable = false, insertable = false, updatable = false)
    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @ManyToOne
    @JoinColumn(
        name = "id_movie_theatre",
        referencedColumnName = "id_movie_theatre",
        nullable = false,
        insertable = false,
        updatable = false
    )
    public MovieTheatre getMovieTheatre() {
        return movieTheatre;
    }

    public void setMovieTheatre(MovieTheatre movieTheatre) {
        this.movieTheatre = movieTheatre;
    }

    @Basic
    @Column(name = "start_date", nullable = false)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "end_date", nullable = false)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoviePosted that = (MoviePosted) o;
        return (Objects.equals(startDate, that.startDate) && Objects.equals(endDate, that.endDate));
    }

    @Override
    public int hashCode() {
        return Objects.hash(startDate, endDate);
    }

    @Id
    @Column(name = "id_movie", nullable = false)
    public Integer getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(Integer idMovie) {
        this.idMovie = idMovie;
    }

    @Id
    @Column(name = "id_movie_theatre", nullable = false)
    public Integer getIdMovieTheatre() {
        return idMovieTheatre;
    }

    public void setIdMovieTheatre(Integer idMovieTheatre) {
        this.idMovieTheatre = idMovieTheatre;
    }
}
