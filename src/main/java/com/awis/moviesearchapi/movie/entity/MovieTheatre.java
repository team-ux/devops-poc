package com.awis.moviesearchapi.movie.entity;

import com.awis.moviesearchapi.user.entity.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name = "movie_theatre", schema = "movie-search")
public class MovieTheatre {

    private Integer idMovieTheatre;
    private String name;
    private Integer streetNumber;
    private String streetName;
    private String city;
    private Integer zipcode;
    private String country;

    private Set<MovieSession> movieSessions;
    private Set<MoviePosted> moviesPosted;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_movie_theatre", nullable = false)
    public Integer getIdMovieTheatre() {
        return idMovieTheatre;
    }

    public void setIdMovieTheatre(Integer idMovieTheatre) {
        this.idMovieTheatre = idMovieTheatre;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "street_number", nullable = false)
    public Integer getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(Integer streetNumber) {
        this.streetNumber = streetNumber;
    }

    @Basic
    @Column(name = "street_name", nullable = false, length = 50)
    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    @Basic
    @Column(name = "city", nullable = false, length = 50)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "zipcode", nullable = false)
    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }

    @Basic
    @Column(name = "country", nullable = false, length = 50)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieTheatre that = (MovieTheatre) o;
        return (
            Objects.equals(idMovieTheatre, that.idMovieTheatre) &&
            Objects.equals(name, that.name) &&
            Objects.equals(streetNumber, that.streetNumber) &&
            Objects.equals(streetName, that.streetName) &&
            Objects.equals(city, that.city) &&
            Objects.equals(zipcode, that.zipcode) &&
            Objects.equals(country, that.country)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMovieTheatre, name, streetNumber, streetName, city, zipcode, country);
    }

    @JsonIgnore
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "movieTheatre", orphanRemoval = true)
    public Set<MovieSession> getMovieSessions() {
        return movieSessions;
    }

    public void setMovieSessions(Set<MovieSession> movieSessions) {
        this.movieSessions = movieSessions;
    }

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "movieTheatre", orphanRemoval = true)
    public Set<MoviePosted> getMoviesPosted() {
        return moviesPosted;
    }

    public void setMoviesPosted(Set<MoviePosted> moviesPosted) {
        this.moviesPosted = moviesPosted;
    }
}
