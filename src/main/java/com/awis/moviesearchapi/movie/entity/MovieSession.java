package com.awis.moviesearchapi.movie.entity;

import java.sql.Timestamp;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "movie_session", schema = "movie-search", catalog = "")
public class MovieSession {

    private Integer sessionId;
    private Timestamp session;
    private MovieTheatre movieTheatre;
    private Movie movie;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "session_id", nullable = false)
    public Integer getSessionId() {
        return sessionId;
    }

    public void setSessionId(Integer sessionId) {
        this.sessionId = sessionId;
    }

    @Basic
    @Column(name = "session", nullable = false)
    public Timestamp getSession() {
        return session;
    }

    public void setSession(Timestamp session) {
        this.session = session;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovieSession that = (MovieSession) o;
        return (Objects.equals(sessionId, that.sessionId) && Objects.equals(session, that.session));
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionId, session);
    }

    @ManyToOne
    @JoinColumn(name = "id_movie_theatre", referencedColumnName = "id_movie_theatre", nullable = false)
    public MovieTheatre getMovieTheatre() {
        return movieTheatre;
    }

    public void setMovieTheatre(MovieTheatre movieTheatre) {
        this.movieTheatre = movieTheatre;
    }

    @ManyToOne
    @JoinColumn(name = "id_movie", referencedColumnName = "id_movie", nullable = false)
    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
