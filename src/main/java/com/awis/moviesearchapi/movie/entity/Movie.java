package com.awis.moviesearchapi.movie.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.*;

@Entity
public class Movie {

    private Integer idMovie;
    private String title;
    private Integer duration;
    private String lang;
    private Boolean subtitles;
    private String director;
    private Integer minAge;
    private Collection<MovieSession> movieSessions;

    private Set<Actor> actorsInMovie = new HashSet<>();
    private Set<MoviePosted> moviesPosted;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_movie", nullable = false)
    public Integer getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(Integer idMovie) {
        this.idMovie = idMovie;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 50)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "duration", nullable = false)
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "lang", nullable = false, length = 50)
    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    @Basic
    @Column(name = "subtitles", nullable = false)
    public Boolean getSubtitles() {
        return subtitles;
    }

    public void setSubtitles(Boolean subtitles) {
        this.subtitles = subtitles;
    }

    @Basic
    @Column(name = "director", nullable = false, length = 50)
    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Basic
    @Column(name = "min_age")
    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return (
            Objects.equals(idMovie, movie.idMovie) &&
            Objects.equals(title, movie.title) &&
            Objects.equals(duration, movie.duration) &&
            Objects.equals(lang, movie.lang) &&
            Objects.equals(subtitles, movie.subtitles) &&
            Objects.equals(director, movie.director) &&
            Objects.equals(minAge, movie.minAge)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMovie, title, duration, lang, subtitles, director, minAge);
    }

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "movie", orphanRemoval = true)
    public Collection<MovieSession> getMovieSessions() {
        return movieSessions;
    }

    public void setMovieSessions(Collection<MovieSession> movieSessions) {
        this.movieSessions = movieSessions;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "actor_in_movie",
        joinColumns = @JoinColumn(name = "id_movie", referencedColumnName = "id_movie"),
        inverseJoinColumns = @JoinColumn(name = "id_actor", referencedColumnName = "id_actor")
    )
    public Set<Actor> getActorsInMovie() {
        return actorsInMovie;
    }

    public void setActorsInMovie(Set<Actor> actorsInMovie) {
        this.actorsInMovie = actorsInMovie;
    }

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "movie", orphanRemoval = true)
    public Set<MoviePosted> getMoviesPosted() {
        return moviesPosted;
    }

    public void setMoviesPosted(Set<MoviePosted> moviesPosted) {
        this.moviesPosted = moviesPosted;
    }

    @Override
    public String toString() {
        return (
            "Movie{" +
            "idMovie=" +
            idMovie +
            ", title='" +
            title +
            '\'' +
            ", duration=" +
            duration +
            ", lang='" +
            lang +
            '\'' +
            ", subtitles=" +
            subtitles +
            ", director='" +
            director +
            '\'' +
            ", minAge=" +
            minAge +
            '}'
        );
    }
}
