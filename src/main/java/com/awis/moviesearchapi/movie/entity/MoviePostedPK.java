package com.awis.moviesearchapi.movie.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Id;

public class MoviePostedPK implements Serializable {

    private Integer idMovie;
    private Integer idMovieTheatre;

    public MoviePostedPK() {}

    public MoviePostedPK(Integer idMovie, Integer idMovieTheatre) {
        this.idMovie = idMovie;
        this.idMovieTheatre = idMovieTheatre;
    }

    @Column(name = "id_movie", nullable = false)
    @Id
    public Integer getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(Integer idMovie) {
        this.idMovie = idMovie;
    }

    @Column(name = "id_movie_theatre", nullable = false)
    @Id
    public Integer getIdMovieTheatre() {
        return idMovieTheatre;
    }

    public void setIdMovieTheatre(Integer idMovieTheatre) {
        this.idMovieTheatre = idMovieTheatre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MoviePostedPK that = (MoviePostedPK) o;
        return (Objects.equals(idMovie, that.idMovie) && Objects.equals(idMovieTheatre, that.idMovieTheatre));
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMovie, idMovieTheatre);
    }
}
