package com.awis.moviesearchapi.movie.api.model;

import com.awis.moviesearchapi.movie.entity.Actor;
import java.sql.Date;
import java.util.List;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Movie model in post request
 */
@XmlRootElement
public class QueryMoviePost {

    private Integer idMovie;
    private String title;
    private Integer duration;
    private String lang;
    private Boolean subtitles;
    private String director;
    private Integer minAge;

    @NotNull
    private Date startDate;

    @NotNull
    private Date endDate;

    private List<Actor> actors;

    @AssertTrue(message = "a property must be exclusively system or owned")
    private boolean isValidMovie() {
        if (
            idMovie == null && title != null && duration != null && lang != null && subtitles != null && director != null && minAge != null
        ) return true; else return (
            idMovie != null && title == null && duration == null && lang == null && subtitles == null && director == null && minAge == null
        );
    }

    public Integer getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(Integer idMovie) {
        this.idMovie = idMovie;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Boolean getSubtitles() {
        return subtitles;
    }

    public void setSubtitles(Boolean subtitles) {
        this.subtitles = subtitles;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public Integer getMinAge() {
        return minAge;
    }

    public void setMinAge(Integer minAge) {
        this.minAge = minAge;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }
}
