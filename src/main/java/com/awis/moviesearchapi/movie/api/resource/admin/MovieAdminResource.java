package com.awis.moviesearchapi.movie.api.resource.admin;

import com.awis.moviesearchapi.movie.service.MoviePostedService;
import com.awis.moviesearchapi.movie.service.MovieService;
import com.awis.moviesearchapi.movie.service.MovieSessionService;
import com.awis.moviesearchapi.security.Secured;
import com.awis.moviesearchapi.security.api.model.Role;
import com.awis.moviesearchapi.user.service.UserService;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/admin/movies")
public class MovieAdminResource {

    private final MovieService movieService;

    private final MoviePostedService moviePostedService;

    private final MovieSessionService movieSessionService;

    private final UserService userService;

    @Autowired
    public MovieAdminResource(
        MovieService movieService,
        MoviePostedService moviePostedService,
        MovieSessionService movieSessionService,
        UserService userService
    ) {
        this.movieService = movieService;
        this.moviePostedService = moviePostedService;
        this.movieSessionService = movieSessionService;
        this.userService = userService;
    }

    /**
     * DELETE a movie from the platform
     *
     * @return Response with all users
     */
    @DELETE
    @Path("{id}")
    @Secured({ Role.ADMIN })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteMoviePosted(@PathParam(value = "id") Integer id, @Context SecurityContext securityContext) {
        movieService.delete(id);
        return Response.ok(String.format("movie posted %s deleted", id)).build();
    }
}
