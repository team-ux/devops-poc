package com.awis.moviesearchapi.movie.api.model;

public class QueryMovieTheatreResult {

    private Integer idMovieTheatre;
    private String name;
    private Integer streetNumber;
    private String streetName;
    private String city;
    private Integer zipcode;
    private String country;

    public Integer getIdMovieTheatre() {
        return idMovieTheatre;
    }

    public void setIdMovieTheatre(Integer idMovieTheatre) {
        this.idMovieTheatre = idMovieTheatre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(Integer streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
