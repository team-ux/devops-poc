package com.awis.moviesearchapi.movie.api.model;

import java.util.List;

public class QueryMovieDetailResult {

    QueryMovieResult movie;
    List<QueryMovieSessionResult> sessions;

    public QueryMovieResult getMovie() {
        return movie;
    }

    public void setMovie(QueryMovieResult movieResult) {
        this.movie = movieResult;
    }

    public List<QueryMovieSessionResult> getSessions() {
        return sessions;
    }

    public void setSessions(List<QueryMovieSessionResult> movieSessions) {
        this.sessions = movieSessions;
    }
}
