package com.awis.moviesearchapi.movie.api.resource;

import static com.awis.moviesearchapi.common.Constant.USER_AUTHENTICATE;

import com.awis.moviesearchapi.movie.api.model.QueryMoviePost;
import com.awis.moviesearchapi.movie.api.model.QueryMovieResult;
import com.awis.moviesearchapi.movie.entity.Movie;
import com.awis.moviesearchapi.movie.entity.MoviePostedPK;
import com.awis.moviesearchapi.movie.service.MoviePostedService;
import com.awis.moviesearchapi.movie.service.MovieService;
import com.awis.moviesearchapi.security.Secured;
import com.awis.moviesearchapi.security.api.model.Role;
import com.awis.moviesearchapi.user.entity.User;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

@Component
@Path("/movies-posted")
public class MoviePostedResource {

    private final MovieService movieService;

    private final MoviePostedService moviePostedService;

    @Context
    HttpServletRequest httpServletRequest;

    @Autowired
    public MoviePostedResource(MovieService movieService, MoviePostedService moviePostedService) {
        this.movieService = movieService;
        this.moviePostedService = moviePostedService;
    }

    /**
     * Get all movies register in database
     *
     * @return Response with all movies posted
     */
    @GET
    @Path("/library")
    @Secured({ Role.MOVIE_OWNER })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMovies(@Context SecurityContext securityContext) {
        Iterable<Movie> movies;
        User user = (User) httpServletRequest.getAttribute(USER_AUTHENTICATE);
        movies = movieService.findAllNotPostedByMovieTheatre(user.getMovieTheatre());
        List<QueryMovieResult> queryDetailsList = StreamSupport
            .stream(movies.spliterator(), false)
            .map(movieService::toQueryResult)
            .collect(Collectors.toList());
        return Response.ok(queryDetailsList).build();
    }

    /**
     * Get all movies posted by an user.
     *
     * @return Response with all movies posted
     */
    @GET
    @Secured({ Role.MOVIE_OWNER })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMoviesPosted(@Context SecurityContext securityContext) {
        Iterable<Movie> movies;
        User user = (User) httpServletRequest.getAttribute(USER_AUTHENTICATE);
        // TODO find by movie theatre
        movies = movieService.findAllByMovieTheatre(user.getMovieTheatre());
        List<QueryMovieResult> queryDetailsList = StreamSupport
            .stream(movies.spliterator(), false)
            .map(movieService::toQueryResult)
            .collect(Collectors.toList());

        return Response.ok(queryDetailsList).build();
    }

    /**
     * POST a new movie by an user
     *
     * @return Response with movie id created
     */
    @POST
    @Secured({ Role.MOVIE_OWNER })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postMovie(@Valid QueryMoviePost movie, @Context SecurityContext securityContext)
        throws DataIntegrityViolationException {
        User user = (User) httpServletRequest.getAttribute(USER_AUTHENTICATE);
        if (user == null) return Response.status(Response.Status.BAD_REQUEST).build();
        if (movie.getIdMovie() != null) {
            MoviePostedPK key = new MoviePostedPK(movie.getIdMovie(), user.getMovieTheatre().getIdMovieTheatre());
            if (moviePostedService.findByMovieId(key).isPresent()) {
                return Response.status(Response.Status.BAD_REQUEST).build();
            } else if (!movieService.existById(movie.getIdMovie())) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        }
        Movie newMovie = movieService.save(movie, user);
        return Response.ok(String.format("movie %s posted !", newMovie.getIdMovie())).build();
    }

    /**
     * DELETE a movie posted by an user
     *
     * @return Response with a success message
     */
    @DELETE
    @Path("{id}")
    @Secured({ Role.MOVIE_OWNER })
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteMoviePosted(@PathParam(value = "id") Integer id, @Context SecurityContext securityContext) {
        User user = (User) httpServletRequest.getAttribute(USER_AUTHENTICATE);
        if (user == null) return Response.status(Response.Status.BAD_REQUEST).build();
        MoviePostedPK key = new MoviePostedPK(id, user.getMovieTheatre().getIdMovieTheatre());
        if (moviePostedService.existById(key)) {
            moviePostedService.delete(key);
            return Response.ok(String.format("movie posted %s deleted", id)).build();
        } else return Response.status(Response.Status.NOT_FOUND).build();
    }
}
