package com.awis.moviesearchapi.movie.api.resource;

import com.awis.moviesearchapi.movie.entity.Actor;
import com.awis.moviesearchapi.movie.service.ActorService;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/actors")
public class ActorResource {

    private final ActorService actorService;

    @Autowired
    public ActorResource(ActorService actorService) {
        this.actorService = actorService;
    }

    /**
     * Get all actors.
     *
     * @return Response with all actors
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllActors() {
        Iterable<Actor> actors;
        actors = actorService.findAll();
        return Response.ok(actors).build();
    }
}
