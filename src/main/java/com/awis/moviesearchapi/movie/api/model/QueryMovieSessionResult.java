package com.awis.moviesearchapi.movie.api.model;

import java.sql.Timestamp;

public class QueryMovieSessionResult {

    Timestamp session;
    QueryMovieTheatreResult movieTheatre;
    String movieTitle;

    public Timestamp getSession() {
        return session;
    }

    public void setSession(Timestamp session) {
        this.session = session;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public QueryMovieTheatreResult getMovieTheatre() {
        return movieTheatre;
    }

    public void setMovieTheatre(QueryMovieTheatreResult movieTheatre) {
        this.movieTheatre = movieTheatre;
    }
}
