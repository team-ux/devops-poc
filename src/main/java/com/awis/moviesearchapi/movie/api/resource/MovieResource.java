package com.awis.moviesearchapi.movie.api.resource;

import com.awis.moviesearchapi.common.exception.ResourceNotFoundException;
import com.awis.moviesearchapi.movie.api.model.QueryMovieDetailResult;
import com.awis.moviesearchapi.movie.api.model.QueryMovieResult;
import com.awis.moviesearchapi.movie.entity.Movie;
import com.awis.moviesearchapi.movie.entity.MoviePosted;
import com.awis.moviesearchapi.movie.entity.MovieSession;
import com.awis.moviesearchapi.movie.service.MoviePostedService;
import com.awis.moviesearchapi.movie.service.MovieService;
import com.awis.moviesearchapi.movie.service.MovieSessionService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@Path("/movies")
public class MovieResource {

    private final MovieService movieService;

    private final MoviePostedService moviePostedService;

    private final MovieSessionService movieSessionService;

    @Context
    HttpServletRequest httpServletRequest;

    @Autowired
    public MovieResource(MovieService movieService, MoviePostedService moviePostedService, MovieSessionService movieSessionService) {
        this.movieService = movieService;
        this.moviePostedService = moviePostedService;
        this.movieSessionService = movieSessionService;
    }

    /**
     * Get all movies.
     *
     * @param city query param to filter by city
     * @return Response with all movie
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMovies(@QueryParam(value = "city") String city) {
        Iterable<Movie> movies;
        movies =
            city == null || city.isEmpty()
                ? StreamSupport
                    .stream(moviePostedService.findAllGroupByMovie().spliterator(), false)
                    .map(MoviePosted::getMovie)
                    .collect(Collectors.toList())
                : movieService.findByCity(city);

        List<QueryMovieResult> queryDetailsList = StreamSupport
            .stream(movies.spliterator(), false)
            .map(movieService::toQueryResult)
            .collect(Collectors.toList());

        return Response.ok(queryDetailsList).build();
    }

    /**
     * GET a movie by id
     *
     * @return Response with movie detail
     */
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMovie(@PathParam(value = "id") Integer id) {
        Optional<Movie> movieOptional = movieService.findById(id);
        if (movieOptional.isPresent()) {
            // get all sessions
            Iterable<MovieSession> movieSessions = movieSessionService.findAllByMovieId(movieOptional.get().getIdMovie());
            QueryMovieDetailResult movieResult = movieService.toQueryDetailResult(movieOptional.get(), movieSessions);
            return Response.ok().entity(movieResult).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
