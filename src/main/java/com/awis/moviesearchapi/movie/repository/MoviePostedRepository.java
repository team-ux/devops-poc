package com.awis.moviesearchapi.movie.repository;

import com.awis.moviesearchapi.movie.entity.MoviePosted;
import com.awis.moviesearchapi.movie.entity.MoviePostedPK;
import com.awis.moviesearchapi.movie.entity.MovieTheatre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface MoviePostedRepository extends JpaRepository<MoviePosted, MoviePostedPK> {
    void deleteAllByMovieIdMovie(Integer movie_idMovie);

    @Query("select mp from MoviePosted mp join User u on mp.movieTheatre <> :movieTheatre group by mp.movie")
    Iterable<MoviePosted> findAllNotPostedByMovieTheatre(MovieTheatre movieTheatre);

    @Query("select mp from MoviePosted mp group by mp.idMovie")
    Iterable<MoviePosted> findAllGroupByMovie();
}
