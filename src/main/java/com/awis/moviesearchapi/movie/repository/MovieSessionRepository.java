package com.awis.moviesearchapi.movie.repository;

import com.awis.moviesearchapi.movie.entity.MovieSession;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for {@link MovieSession}s.
 */
public interface MovieSessionRepository extends JpaRepository<MovieSession, Integer> {
    Iterable<MovieSession> findAllByMovieIdMovie(Integer movie_idMovie);

    void deleteAllByMovieIdMovieAndMovieTheatreIdMovieTheatre(Integer movie_idMovie, Integer movieTheatre_idMovieTheatre);
}
