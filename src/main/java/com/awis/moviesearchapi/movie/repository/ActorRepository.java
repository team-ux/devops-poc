package com.awis.moviesearchapi.movie.repository;

import com.awis.moviesearchapi.movie.entity.Actor;
import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ActorRepository extends JpaRepository<Actor, Integer> {
    Iterable<Actor> findAllByNameInAndSurnameIn(Collection<String> name, Collection<String> surname);

    @Modifying
    @Query("update Actor a set a.name = :name, a.surname = :surname where a.idActor = :id")
    void setActorFullName(String name, String surname, Integer id);
}
