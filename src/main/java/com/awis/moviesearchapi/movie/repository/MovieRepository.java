package com.awis.moviesearchapi.movie.repository;

import com.awis.moviesearchapi.movie.entity.Movie;
import com.awis.moviesearchapi.movie.entity.MovieTheatre;
import com.awis.moviesearchapi.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for {@link Movie}s.
 */
public interface MovieRepository extends JpaRepository<Movie, Integer> {
    @Query(
        "select m from Movie m join MoviePosted mp on m.idMovie = mp.movie.idMovie where mp.movieTheatre.city = :city group by m.idMovie"
    )
    public Iterable<Movie> getMovieByCity(String city);

    @Query("select m.movie from MoviePosted m join MovieTheatre mt on mt.idMovieTheatre = m.idMovieTheatre where mt = :#{#movieTheatre}")
    Iterable<Movie> findAllByMovieTheatre(@Param("movieTheatre") MovieTheatre movieTheatre);

    @Query("select m from Movie m where m not in (select mp.movie from MoviePosted mp where mp.movieTheatre = :#{#movieTheatre})")
    Iterable<Movie> findAllNotPostedByMovieTheatre(@Param("movieTheatre") MovieTheatre movieTheatre);
}
