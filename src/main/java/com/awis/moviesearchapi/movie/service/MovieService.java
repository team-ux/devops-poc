package com.awis.moviesearchapi.movie.service;

import com.awis.moviesearchapi.common.exception.ResourceNotFoundException;
import com.awis.moviesearchapi.movie.api.model.QueryMovieDetailResult;
import com.awis.moviesearchapi.movie.api.model.QueryMoviePost;
import com.awis.moviesearchapi.movie.api.model.QueryMovieResult;
import com.awis.moviesearchapi.movie.api.model.QueryMovieSessionResult;
import com.awis.moviesearchapi.movie.entity.*;
import com.awis.moviesearchapi.movie.repository.MovieRepository;
import com.awis.moviesearchapi.user.entity.User;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.ws.rs.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MovieService {

    private final MovieRepository movieRepository;

    private final MovieSessionService movieSessionService;

    private final MoviePostedService moviePostedService;

    private final ActorService actorService;

    @Autowired
    public MovieService(
        MovieRepository movieRepository,
        MovieSessionService movieSessionService,
        MoviePostedService moviePostedService,
        ActorService actorService
    ) {
        this.movieRepository = movieRepository;
        this.movieSessionService = movieSessionService;
        this.moviePostedService = moviePostedService;
        this.actorService = actorService;
    }

    public Iterable<Movie> findAll() {
        return movieRepository.findAll();
    }

    public Iterable<Movie> findAllByMovieTheatre(MovieTheatre movieTheatre) {
        return movieRepository.findAllByMovieTheatre(movieTheatre);
    }

    public Iterable<Movie> findAllNotPostedByMovieTheatre(MovieTheatre movieTheatre) {
        return this.movieRepository.findAllNotPostedByMovieTheatre(movieTheatre);
    }

    public Iterable<Movie> findByCity(String city) {
        return movieRepository.getMovieByCity(city);
    }

    public Optional<Movie> findById(Integer id) {
        return movieRepository.findById(id);
    }

    public boolean existById(Integer id) {
        return movieRepository.existsById(id);
    }

    /**
     * Save a movie
     *
     * @param movie the query movie from body request
     * @param owner the secured user who wants to save
     * @return the movie save
     */
    @Transactional
    public Movie save(QueryMoviePost movie, User owner) throws NotFoundException, DataIntegrityViolationException {
        Movie newMovie;
        if (movie.getIdMovie() == null) newMovie = toMovie(movie); else newMovie =
            movieRepository
                .findById(movie.getIdMovie())
                .orElseThrow(() -> new NotFoundException("Error movie " + movie.getIdMovie() + " not found"));
        System.out.println(newMovie);
        List<Actor> actors = movie.getActors();

        System.out.println(movie.getActors());
        // save new actors before save movie
        actorService.saveAndFlush(actors.stream().filter(actor -> actor.getIdActor() == null).collect(Collectors.toList()));

        newMovie.setActorsInMovie(new HashSet<>(actors));

        // add new movie in reference
        actors.forEach(actor -> actor.getMovies().add(newMovie));

        if (newMovie.getIdMovie() == null) {
            movieRepository.saveAndFlush(newMovie);
        }

        MoviePosted moviePosted = new MoviePosted(newMovie.getIdMovie(), owner.getMovieTheatre().getIdMovieTheatre());
        moviePosted.setStartDate(movie.getStartDate());
        moviePosted.setEndDate(movie.getEndDate());

        moviePostedService.save(moviePosted);

        List<MovieSession> sessions = movieSessionService.createSessions(newMovie, owner.getMovieTheatre(), moviePosted);
        this.movieSessionService.saveAll(sessions);

        return newMovie;
    }

    /**
     * Delete a movie
     *
     * @param id the move id
     */
    @Transactional
    public void delete(Integer id) {
        moviePostedService.deleteAllByMovieId(id);
        Optional<Movie> movie = movieRepository.findById(id);
        if (movie.isPresent()) {
            movie.get().getActorsInMovie().clear();
            movieRepository.delete(movie.get());
        }
    }

    /**
     * Map a {@link QueryMoviePost} instance to a {@link Movie} instance.
     *
     * @param movie the movie instance
     * @return query result
     */
    public Movie toMovie(QueryMoviePost movie) {
        Movie result = new Movie();
        result.setIdMovie(movie.getIdMovie());
        result.setTitle(movie.getTitle());
        result.setLang(movie.getLang());
        result.setDirector(movie.getDirector());
        result.setMinAge(movie.getMinAge());
        result.setDuration(movie.getDuration());
        result.setSubtitles(movie.getSubtitles());
        return result;
    }

    /**
     * Map a {@link Movie} instance to a {@link QueryMovieResult} instance.
     *
     * @param movie the movie instance
     * @return query result
     */
    public QueryMovieResult toQueryResult(Movie movie) {
        QueryMovieResult result = new QueryMovieResult();
        result.setIdMovie(movie.getIdMovie());
        result.setTitle(movie.getTitle());
        result.setLang(movie.getLang());
        result.setDirector(movie.getDirector());
        result.setMinAge(movie.getMinAge());
        result.setDuration(movie.getDuration());
        result.setSubtitles(movie.getSubtitles());
        result.setActors(movie.getActorsInMovie());
        return result;
    }

    public QueryMovieDetailResult toQueryDetailResult(Movie movie, Iterable<MovieSession> movieSessions) {
        QueryMovieResult queryMovieResult = toQueryResult(movie);
        List<QueryMovieSessionResult> movieSessionResults = StreamSupport
            .stream(movieSessions.spliterator(), false)
            .map(movieSessionService::toResult)
            .collect(Collectors.toList());
        QueryMovieDetailResult detailResult = new QueryMovieDetailResult();
        detailResult.setMovie(queryMovieResult);
        detailResult.setSessions(movieSessionResults);
        return detailResult;
    }
}
