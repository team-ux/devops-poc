package com.awis.moviesearchapi.movie.service;

import com.awis.moviesearchapi.movie.entity.Actor;
import com.awis.moviesearchapi.movie.repository.ActorRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActorService {

    private final ActorRepository actorRepository;

    @Autowired
    public ActorService(ActorRepository actorRepository) {
        this.actorRepository = actorRepository;
    }

    public void save(Actor actor) {
        actorRepository.save(actor);
    }

    public void saveAll(List<Actor> actors) {
        actorRepository.saveAll(actors);
    }

    public Iterable<Actor> findAllByFullName(List<Actor> actors) {
        return actorRepository.findAllById(actors.stream().map(Actor::getIdActor).collect(Collectors.toList()));
    }

    public Iterable<Actor> findAll() {
        return actorRepository.findAll();
    }

    public void saveAndFlush(List<Actor> actors) {
        actors.forEach(actorRepository::saveAndFlush);
    }
}
