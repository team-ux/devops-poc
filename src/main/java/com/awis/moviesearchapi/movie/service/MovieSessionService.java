package com.awis.moviesearchapi.movie.service;

import com.awis.moviesearchapi.movie.api.model.QueryMovieSessionResult;
import com.awis.moviesearchapi.movie.api.model.QueryMovieTheatreResult;
import com.awis.moviesearchapi.movie.entity.*;
import com.awis.moviesearchapi.movie.repository.MovieSessionRepository;
import java.sql.Timestamp;
import java.time.DateTimeException;
import java.util.*;
import java.util.stream.Collectors;
import org.joda.time.DateTime;
import org.joda.time.Weeks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieSessionService {

    private static final int NUMBER_MOVIE_PER_WEEK = 3;
    private static final int DAYS_PER_WEEK = 7;
    private static final List<String> sessionsHours = Arrays.asList(
        "10:00:00",
        "11:00:00",
        "15:00:00",
        "17:00:00",
        "19:00:00",
        "20:00:00",
        "21:00:00",
        "22:00:00"
    );
    private final MovieSessionRepository movieSessionRepository;

    @Autowired
    public MovieSessionService(MovieSessionRepository movieSessionRepository) {
        this.movieSessionRepository = movieSessionRepository;
    }

    public Iterable<MovieSession> findAllByMovieId(Integer movieId) {
        return movieSessionRepository.findAllByMovieIdMovie(movieId);
    }

    public void deleteAllByMovieAndMovieTheatre(MoviePostedPK moviePostedPK) {
        movieSessionRepository.deleteAllByMovieIdMovieAndMovieTheatreIdMovieTheatre(
            moviePostedPK.getIdMovie(),
            moviePostedPK.getIdMovieTheatre()
        );
    }

    public void saveAll(List<MovieSession> sessions) {
        this.movieSessionRepository.saveAll(sessions);
    }

    public List<MovieSession> createSessions(Movie movie, MovieTheatre movieTheatre, MoviePosted moviePosted) throws DateTimeException {
        List<MovieSession> movieSessions = new ArrayList<>();
        int weeks = countNumberOfWeek(moviePosted.getStartDate(), moviePosted.getEndDate());
        System.out.println(weeks);
        DateTime start = new DateTime(moviePosted.getStartDate());
        for (int i = 0; i < weeks; i++) {
            // get random sessions during a week
            List<Timestamp> sessions = generateRandomSessionsPerWeek(start.toDate());
            sessions.forEach(
                session -> {
                    // create a new session
                    MovieSession movieSession = new MovieSession();
                    movieSession.setMovie(movie);
                    movieSession.setMovieTheatre(movieTheatre);
                    movieSession.setSession(session);
                    System.out.println(movieSession);
                    movieSessions.add(movieSession);
                }
            );
            // add a week
            start = start.plusWeeks(1);
        }
        return movieSessions;
    }

    /**
     * Count the number of week between two days
     *
     * @param startDate the start date of movie release
     * @param endDate   the end date of movie
     * @return the number of weeks
     * @throws DateTimeException if error during parsing date
     */
    public int countNumberOfWeek(Date startDate, Date endDate) throws DateTimeException {
        DateTime start = new DateTime(startDate);
        DateTime end = new DateTime(endDate);
        /*ZonedDateTime start = Instant.ofEpochMilli(startDate.getTime()).atZone(zoneId);
        ZonedDateTime end = Instant.ofEpochMilli(endDate.getTime()).atZone(zoneId);*/
        System.out.println(start);
        System.out.println(end);
        return Weeks.weeksBetween(start, end).getWeeks();
    }

    /**
     * Generate random sessions during a week
     *
     * @param start
     * @return a list of sessions represented by timestamps
     */
    public List<Timestamp> generateRandomSessionsPerWeek(Date start) {
        List<Timestamp> sessions = new ArrayList<>();
        // get days in the week
        Set<Integer> days = generateRandomDay();
        days.forEach(
            day -> {
                List<Integer> times = pickARandomSessionTime();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(start);
                calendar.add(Calendar.DATE, day);
                calendar.set(Calendar.HOUR_OF_DAY, times.get(0));
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                sessions.add(new Timestamp(calendar.getTimeInMillis()));
            }
        );
        return sessions;
    }

    /**
     * Pick index of days during a week
     *
     * @return a list of index which represent a day in a week
     */
    public Set<Integer> generateRandomDay() {
        Set<Integer> days = new TreeSet<>();
        Random rng = new Random();
        while (days.size() < NUMBER_MOVIE_PER_WEEK) {
            Integer next = rng.nextInt(DAYS_PER_WEEK);
            days.add(next);
        }
        return days;
    }

    /**
     * Pick a random time to a session
     *
     * @return a list of integer which represent the time "hour" - "minute" - "second"
     */
    public List<Integer> pickARandomSessionTime() {
        Random rng = new Random();
        return Arrays
            .stream(sessionsHours.get(rng.nextInt(sessionsHours.size())).split(":"))
            .map(Integer::valueOf)
            .collect(Collectors.toList());
    }

    public QueryMovieSessionResult toResult(MovieSession movieSession) {
        QueryMovieSessionResult result = new QueryMovieSessionResult();
        result.setSession(movieSession.getSession());
        result.setMovieTitle(movieSession.getMovie().getTitle());
        QueryMovieTheatreResult movieTheatreResult = new QueryMovieTheatreResult();
        movieTheatreResult.setIdMovieTheatre(movieSession.getMovieTheatre().getIdMovieTheatre());
        movieTheatreResult.setName(movieSession.getMovieTheatre().getName());
        movieTheatreResult.setCity(movieSession.getMovieTheatre().getCity());
        movieTheatreResult.setCountry(movieSession.getMovieTheatre().getCountry());
        movieTheatreResult.setZipcode(movieSession.getMovieTheatre().getZipcode());
        movieTheatreResult.setStreetName(movieSession.getMovieTheatre().getStreetName());
        movieTheatreResult.setStreetNumber(movieSession.getMovieTheatre().getStreetNumber());
        result.setMovieTheatre(movieTheatreResult);
        return result;
    }
}
