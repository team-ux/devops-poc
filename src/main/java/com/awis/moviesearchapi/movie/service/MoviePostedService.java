package com.awis.moviesearchapi.movie.service;

import com.awis.moviesearchapi.movie.entity.Movie;
import com.awis.moviesearchapi.movie.entity.MoviePosted;
import com.awis.moviesearchapi.movie.entity.MoviePostedPK;
import com.awis.moviesearchapi.movie.entity.MovieTheatre;
import com.awis.moviesearchapi.movie.repository.MoviePostedRepository;
import com.awis.moviesearchapi.user.entity.User;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MoviePostedService {

    private final MoviePostedRepository moviePostedRepository;

    @Autowired
    private MovieSessionService movieSessionService;

    @Autowired
    public MoviePostedService(MoviePostedRepository moviePostedRepository) {
        this.moviePostedRepository = moviePostedRepository;
    }

    public Iterable<MoviePosted> findAllGroupByMovie() {
        return moviePostedRepository.findAllGroupByMovie();
    }

    public Optional<MoviePosted> findByMovieId(MoviePostedPK moviePostedPK) {
        return moviePostedRepository.findById(moviePostedPK);
    }

    public boolean existById(MoviePostedPK moviePostedPK) {
        return moviePostedRepository.existsById(moviePostedPK);
    }

    public void save(MoviePosted moviePosted) throws DataIntegrityViolationException {
        this.moviePostedRepository.save(moviePosted);
    }

    @Transactional
    public void delete(MoviePostedPK moviePostedPK) {
        this.moviePostedRepository.deleteById(moviePostedPK);
        this.movieSessionService.deleteAllByMovieAndMovieTheatre(moviePostedPK);
    }

    public void deleteAllByMovieId(Integer movieId) {
        this.moviePostedRepository.deleteAllByMovieIdMovie(movieId);
    }

    public Iterable<Movie> findAllNotPostedByMovieTheatre(MovieTheatre movieTheatre) {
        return StreamSupport
            .stream(this.moviePostedRepository.findAllNotPostedByMovieTheatre(movieTheatre).spliterator(), false)
            .map(MoviePosted::getMovie)
            .collect(Collectors.toList());
    }
}
