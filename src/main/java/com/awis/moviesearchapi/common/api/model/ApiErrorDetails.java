package com.awis.moviesearchapi.common.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import javax.ws.rs.core.Response;

/**
 * API model that represents an error.
 */
@JsonInclude(Include.NON_NULL)
public class ApiErrorDetails {

    private Response.Status status;
    private String title;
    private String message;

    public ApiErrorDetails(Response.Status status, String title, String message) {
        this.status = status;
        this.title = title;
        this.message = message;
    }

    public Response.Status getStatus() {
        return status;
    }

    public void setStatus(Response.Status status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
