package com.awis.moviesearchapi.common.api.provider;

import com.awis.moviesearchapi.common.api.model.ApiErrorDetails;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.springframework.dao.DataIntegrityViolationException;

@Provider
public class DataIntegrityViolationMapper implements ExceptionMapper<DataIntegrityViolationException> {

    @Override
    public Response toResponse(final DataIntegrityViolationException exception) {
        return Response
            .status(Response.Status.INTERNAL_SERVER_ERROR)
            .entity(prepareMessage(exception))
            .type(MediaType.APPLICATION_JSON_TYPE)
            .build();
    }

    private ApiErrorDetails prepareMessage(DataIntegrityViolationException exception) {
        return new ApiErrorDetails(
            Response.Status.INTERNAL_SERVER_ERROR,
            Response.Status.INTERNAL_SERVER_ERROR.getReasonPhrase(),
            exception.getMessage()
        );
    }
}
