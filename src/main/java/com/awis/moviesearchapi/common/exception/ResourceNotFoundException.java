package com.awis.moviesearchapi.common.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ResourceNotFoundException extends Exception implements ExceptionMapper<ResourceNotFoundException> {

    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException(String message) {
        super(message);
    }

    @Override
    public Response toResponse(ResourceNotFoundException e) {
        return Response.status(404).entity(e.getMessage()).type("application/json").build();
    }
}
