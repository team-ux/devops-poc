package com.awis.moviesearchapi.common.validation;

import com.awis.moviesearchapi.movie.api.model.QueryMoviePost;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MoviePostValidator implements ConstraintValidator<MoviePostConstraint, QueryMoviePost> {

    private QueryMoviePost movie;

    @Override
    public void initialize(MoviePostConstraint constraintAnnotation) {}

    @Override
    public boolean isValid(QueryMoviePost movie, ConstraintValidatorContext constraintValidatorContext) {
        if (
            movie.getIdMovie() == null &&
            movie.getTitle() != null &&
            movie.getDuration() != null &&
            movie.getLang() != null &&
            movie.getSubtitles() != null &&
            movie.getDirector() != null &&
            movie.getMinAge() != null
        ) return true; else return (
            movie.getIdMovie() != null &&
            movie.getTitle() == null &&
            movie.getDuration() == null &&
            movie.getLang() == null &&
            movie.getSubtitles() == null &&
            movie.getDirector() == null &&
            movie.getMinAge() == null
        );
    }
}
