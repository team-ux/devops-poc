package com.awis.moviesearchapi.common.validation;

import java.lang.annotation.*;
import javax.validation.Constraint;

@Documented
@Constraint(validatedBy = MoviePostValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MoviePostConstraint {
    String message() default "Fields values don't match!";
}
