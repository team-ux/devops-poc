package com.awis.moviesearchapi.common.config;

import com.awis.moviesearchapi.common.CORSFilter;
import com.awis.moviesearchapi.common.api.provider.ConstraintExceptionMapper;
import com.awis.moviesearchapi.common.api.provider.DataIntegrityViolationMapper;
import com.awis.moviesearchapi.common.api.provider.ObjectMapperProvider;
import com.awis.moviesearchapi.movie.api.resource.ActorResource;
import com.awis.moviesearchapi.movie.api.resource.MoviePostedResource;
import com.awis.moviesearchapi.movie.api.resource.MovieResource;
import com.awis.moviesearchapi.movie.api.resource.admin.MovieAdminResource;
import com.awis.moviesearchapi.security.AuthenticationFilter;
import com.awis.moviesearchapi.security.api.resource.AuthenticationResource;
import com.awis.moviesearchapi.user.api.resource.UserResource;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("api")
public class JerseyConfiguration extends ResourceConfig {

    public JerseyConfiguration() {
        register(UserResource.class);
        register(MovieResource.class);
        register(MoviePostedResource.class);
        register(ActorResource.class);
        register(MovieAdminResource.class);
        register(AuthenticationResource.class);
        register(ObjectMapperProvider.class);
        register(DataIntegrityViolationMapper.class);
        register(ConstraintExceptionMapper.class);
        // register(ResourceNotFoundException.class);
        register(CORSFilter.class);
        register(AuthenticationFilter.class);
    }
}
