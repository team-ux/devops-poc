package com.awis.moviesearchapi.user.api.resource;

import com.awis.moviesearchapi.security.Secured;
import com.awis.moviesearchapi.security.api.model.Role;
import com.awis.moviesearchapi.user.api.model.QueryUserResult;
import com.awis.moviesearchapi.user.entity.User;
import com.awis.moviesearchapi.user.service.UserService;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * JAX-RS resource class for users.
 */
@Component
@Path("users")
public class UserResource {

    private final UserService userService;

    @Context
    private UriInfo uriInfo;

    @Autowired
    public UserResource(UserService userService) {
        this.userService = userService;
    }

    /**
     * Get all users.
     *
     * @return Response with all users
     */
    @GET
    @Secured({ Role.ADMIN })
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {
        Iterable<User> iterable = userService.findAll();
        List<QueryUserResult> queryDetailsList = StreamSupport
            .stream(iterable.spliterator(), false)
            .map(this::toQueryResult)
            .collect(Collectors.toList());

        return Response.ok(queryDetailsList).build();
    }

    /**
     * Map a {@link User} instance to a {@link QueryUserResult} instance.
     *
     * @param user
     * @return
     */
    private QueryUserResult toQueryResult(User user) {
        QueryUserResult result = new QueryUserResult();
        result.setIdUser(user.getIdUser());
        result.setEmail(user.getEmail());
        result.setRole(user.getRole());
        return result;
    }
}
