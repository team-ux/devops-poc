package com.awis.moviesearchapi.user.repository;

import com.awis.moviesearchapi.user.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for {@link User}s.
 */
public interface UserRepository extends CrudRepository<User, Integer> {
    public User findByEmail(String email);

    public User getUserByEmailAndPassword(String email, String password);
}
