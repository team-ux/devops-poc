package com.awis.moviesearchapi.user.entity;

import com.awis.moviesearchapi.movie.entity.MovieTheatre;
import java.util.Objects;
import javax.persistence.*;

@Entity
public class User {

    private Integer idUser;
    private String email;
    private String password;
    private String role;
    private MovieTheatre movieTheatre;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_user", nullable = false)
    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    @Basic
    @Column(name = "email", nullable = false, length = 50)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 50)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "role", nullable = false, length = 50)
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return (
            Objects.equals(idUser, user.idUser) &&
            Objects.equals(email, user.email) &&
            Objects.equals(password, user.password) &&
            Objects.equals(role, user.role)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUser, email, password, role);
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_movie_theatre", referencedColumnName = "id_movie_theatre")
    public MovieTheatre getMovieTheatre() {
        return movieTheatre;
    }

    public void setMovieTheatre(MovieTheatre movieTheatre) {
        this.movieTheatre = movieTheatre;
    }

    @Override
    public String toString() {
        return (
            "User{" + "idUser=" + idUser + ", email='" + email + '\'' + ", password='" + password + '\'' + ", role='" + role + '\'' + '}'
        );
    }
}
