delete from user where id_user > 0;
delete from movie_session where id_movie_theatre > 0;
delete from movie_posted where id_movie_theatre > 0;
delete from movie_theatre where id_movie_theatre > 0;
insert into movie_theatre (id_movie_theatre, name, street_number, street_name, city, zipcode, country)
values (1, 'UGC Bercy', 2, 'Boulevard Poissonnière', 'Paris', 75012, 'France');
insert into movie_theatre (id_movie_theatre, name, street_number, street_name, city, zipcode, country)
values (2, 'MK2 Bibliothèque', 162, 'Boulevard Poissonnière', 'Paris', 75002, 'France');
insert into movie_theatre (id_movie_theatre, name, street_number, street_name, city, zipcode, country)
values (3, 'Pathé Lyon - Bellecour', 69, 'Rue de la République', 'Lyon', 69002, 'France');
insert into movie_theatre (id_movie_theatre, name, street_number, street_name, city, zipcode, country)
values (4, 'Grand Rex', 1, 'Rue d''Odessa', 'Paris', 75013, 'France');

insert into user (id_user, email, password, role, id_movie_theatre) values (1, 'test-paris@m.com', '123', 'MOVIE_OWNER', 1);
insert into user (id_user, email, password, role, id_movie_theatre) values (2, 'larmer1@lycos.com', 'pPSZYo3', 'MOVIE_OWNER', 2);
insert into user (id_user, email, password, role, id_movie_theatre) values (3, 'test-lyon@m.com', '123', 'MOVIE_OWNER', 3);
insert into user (id_user, email, password, role, id_movie_theatre) values (4, 'hmcmurrugh3@google.co.uk', 'Ki97JGcRLnlp', 'MOVIE_OWNER', 4);
insert into user (id_user, email, password, role) values (5, 'admin@m.com', '123', 'ADMIN');