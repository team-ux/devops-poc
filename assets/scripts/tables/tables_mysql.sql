#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: actor
#------------------------------------------------------------

CREATE TABLE actor(
                      id_actor Int  Auto_increment  NOT NULL ,
                      name     Varchar (50) NOT NULL ,
                      surname  Varchar (50) NOT NULL
    ,CONSTRAINT actor_PK PRIMARY KEY (id_actor)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: movie theatre
#------------------------------------------------------------

CREATE TABLE movie_theatre(
                              id_movie_theatre Int  Auto_increment  NOT NULL ,
                              name             Varchar (50) NOT NULL ,
                              street_number    Int NOT NULL ,
                              street_name      Varchar (50) NOT NULL ,
                              city             Varchar (50) NOT NULL ,
                              zipcode          Int NOT NULL ,
                              country          Varchar (50) NOT NULL
    ,CONSTRAINT movie_theatre_PK PRIMARY KEY (id_movie_theatre)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: user
#------------------------------------------------------------

CREATE TABLE user(
                     id_user          Int  Auto_increment  NOT NULL ,
                     email            Varchar (50) NOT NULL ,
                     password         Varchar (50) NOT NULL ,
                     role             Varchar (50) NOT NULL ,
                     id_movie_theatre Int NOT NULL
    ,CONSTRAINT user_PK PRIMARY KEY (id_user)

    ,CONSTRAINT user_movie_theatre_FK FOREIGN KEY (id_movie_theatre) REFERENCES movie_theatre(id_movie_theatre)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: movie
#------------------------------------------------------------

CREATE TABLE movie(
                      id_movie   Int  Auto_increment  NOT NULL ,
                      title      Varchar (50) NOT NULL ,
                      duration   Int NOT NULL ,
                      lang       Varchar (50) NOT NULL ,
                      subtitles  Bool NOT NULL ,
                      director   Varchar (50) NOT NULL ,
                      min_age    Int NOT NULL ,
                      start_date Date NOT NULL ,
                      end_date   Date NOT NULL ,
                      id_user    Int NOT NULL
    ,CONSTRAINT movie_PK PRIMARY KEY (id_movie)

    ,CONSTRAINT movie_user_FK FOREIGN KEY (id_user) REFERENCES user(id_user)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: movie session
#------------------------------------------------------------

CREATE TABLE movie_session(
                              id_movie_session Int  Auto_increment  NOT NULL ,
                              session_datetime Datetime NOT NULL ,
                              id_movie_theatre Int NOT NULL ,
                              id_movie         Int NOT NULL
    ,CONSTRAINT movie_session_PK PRIMARY KEY (id_movie_session)

    ,CONSTRAINT movie_session_movie_theatre_FK FOREIGN KEY (id_movie_theatre) REFERENCES movie_theatre(id_movie_theatre)
    ,CONSTRAINT movie_session_movie0_FK FOREIGN KEY (id_movie) REFERENCES movie(id_movie)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: movie actor
#------------------------------------------------------------

CREATE TABLE movie_actor(
                            id_actor Int NOT NULL ,
                            id_movie Int NOT NULL
    ,CONSTRAINT movie_actor_PK PRIMARY KEY (id_actor,id_movie)

    ,CONSTRAINT movie_actor_actor_FK FOREIGN KEY (id_actor) REFERENCES actor(id_actor)
    ,CONSTRAINT movie_actor_movie0_FK FOREIGN KEY (id_movie) REFERENCES movie(id_movie)
)ENGINE=InnoDB;

