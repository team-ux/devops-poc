#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: movie
#------------------------------------------------------------

CREATE TABLE movie(
        id_movie  Int  Auto_increment  NOT NULL ,
        title     Varchar (50) NOT NULL ,
        duration  Int NOT NULL ,
        lang      Varchar (50) NOT NULL ,
        subtitles Bool NOT NULL ,
        director  Varchar (50) NOT NULL ,
        min_age   Int NOT NULL
	,CONSTRAINT movie_PK PRIMARY KEY (id_movie)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: actor
#------------------------------------------------------------

CREATE TABLE actor(
        id_actor Int  Auto_increment  NOT NULL ,
        name     Varchar (50) NOT NULL ,
        surname  Varchar (50) NOT NULL
	,CONSTRAINT actor_PK PRIMARY KEY (id_actor)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: movie theatre
#------------------------------------------------------------

CREATE TABLE movie_theatre(
        id_movie_theatre Int  Auto_increment  NOT NULL ,
        name             Varchar (50) NOT NULL ,
        street_number    Int NOT NULL ,
        street_name      Varchar (50) NOT NULL ,
        city             Varchar (50) NOT NULL ,
        zipcode          Int NOT NULL ,
        country          Varchar (50) NOT NULL
	,CONSTRAINT movie_theatre_PK PRIMARY KEY (id_movie_theatre)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: user
#------------------------------------------------------------

CREATE TABLE user(
        id_user          Int  Auto_increment  NOT NULL ,
        email            Varchar (50) NOT NULL ,
        password         Varchar (50) NOT NULL ,
        role             Varchar (50) NOT NULL ,
        id_movie_theatre Int
	,CONSTRAINT user_PK PRIMARY KEY (id_user)

	,CONSTRAINT user_movie_theatre_FK FOREIGN KEY (id_movie_theatre) REFERENCES movie_theatre(id_movie_theatre)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: movie session
#------------------------------------------------------------

CREATE TABLE movie_session(
        session_id       Int  Auto_increment  NOT NULL ,
        session          Datetime NOT NULL ,
        id_movie_theatre Int NOT NULL ,
        id_movie         Int NOT NULL
	,CONSTRAINT movie_session_PK PRIMARY KEY (session_id)

	,CONSTRAINT movie_session_movie_theatre_FK FOREIGN KEY (id_movie_theatre) REFERENCES movie_theatre(id_movie_theatre)
	,CONSTRAINT movie_session_movie0_FK FOREIGN KEY (id_movie) REFERENCES movie(id_movie)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: movie posted
#------------------------------------------------------------

CREATE TABLE movie_posted(
        id_movie         Int NOT NULL ,
        id_movie_theatre Int NOT NULL ,
        start_date       Datetime NOT NULL ,
        end_date         Datetime NOT NULL
	,CONSTRAINT movie_posted_PK PRIMARY KEY (id_movie,id_movie_theatre)

	,CONSTRAINT movie_posted_movie_FK FOREIGN KEY (id_movie) REFERENCES movie(id_movie)
	,CONSTRAINT movie_posted_movie_theatre0_FK FOREIGN KEY (id_movie_theatre) REFERENCES movie_theatre(id_movie_theatre)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: actor_in_movie
#------------------------------------------------------------

CREATE TABLE actor_in_movie(
        id_movie Int NOT NULL ,
        id_actor Int NOT NULL
	,CONSTRAINT actor_in_movie_PK PRIMARY KEY (id_movie,id_actor)

	,CONSTRAINT actor_in_movie_movie_FK FOREIGN KEY (id_movie) REFERENCES movie(id_movie)
	,CONSTRAINT actor_in_movie_actor0_FK FOREIGN KEY (id_actor) REFERENCES actor(id_actor)
)ENGINE=InnoDB;

