insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (1, '1917', 118, 'VOSTFR', true, 'Sam Mendes', 10, '2020-12-10', '2020-08-07', 1);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (2, 'Jojo Rabbit', 108, 'VOSTFR', false, 'Taika Waititi', 0, '2020-12-05', '2020-08-26', 2);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (3, 'The Gentlemen', 113, 'VOSTFR', true, 'Guy Ritchie', 10, '2020-11-01', '2020-11-27', 3);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (4, 'Tenet', 150, 'VOSTFR', true, 'Christopher Nolan', 0, '2020-11-23', '2020-08-03', 4);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (5, 'Le Cas Richard Jewell', 131, 'VOSTFR', false, 'Clint Eastwood', 10, '2020-11-10', '2020-11-06', 1);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (6, 'Drunk', 115, 'VOSTFR', false, 'Thomas Vinterberg', 0, '2020-12-07', '2020-03-16', 2);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (7, 'En Avant', 102, 'VOSTFR', true, 'Dan Scanlon', 0, '2020-12-03', '2020-02-28', 3);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (8, 'Ete 85', 100, 'VOSTFR', true, 'François Ozon', 0, '2020-11-07', '2020-05-05', 4);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (9, 'Un pays qui se tient sage', 86, 'FR', false, 'David Dufresne', 10, '2020-11-14', '2020-08-26', 1);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (10, 'Tout simplement noir', 90, 'FR', false, 'Jean-Pascal Zadi & John Wax', 0, '2020-11-24', '2020-11-16', 2);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (11, 'Scandale', 109, 'VOSTFR', true, 'Jay Roach', 0, '2020-11-23', '2020-08-20', 3);
insert into movie (id_movie, title, duration, lang, subtitles, director, min_age, start_date, end_date, id_movie_owner)
values (12, 'La Voie de la justice', 136, 'VOSTFR', true, 'Destin Daniel Crettoner', 0, '2020-12-08', '2020-12-14', 4);
