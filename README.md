# DevOps POC
This POC is an example of GitLab CI/CD
We use a project of REST API to test this pipeline
The deploy step is comment, but we can use this configuration to deploy a container to a server with
SSH access.

## Teams

- Guillaume Thomas
- Wieczorek Antonin
- Doe Victoria
- Ben Slama Amine
- Hilaire François-Xavier

## Stages

- build
- test
- publish 

# Movie Search API
REST API to find movies in your city !
Project of AIWS Efrei Paris 2020   
https://movie-search.donthomz.me/api


## Software stacks
- Jersey  2
- Spring Boot
- Spring Boot JPA (Hibernate)
- MySQL  5.7

## Resources
REST Doc API  
https://documenter.getpostman.com/view/12944415/TVt18jQW#f24422df-3b86-4c18-9f77-55708faad0ef